<#macro bpTree children>
  <#if children?has_content>
      <#list children as child>
      	  <li>
          <#if child.href?has_content>
                        		<a class="menuc" url="${ctx }${child.href}" data-addtab="menuc_id_${child.id}"
	title="${child.name }" href="javascript:;">${child.name }</a>
                        	<#else>
	                            <a href="javascript:;">${child.name } <#if child.children?has_content><span class="fa arrow"></span></#if></a>
                        	</#if>
              <#if child.children?has_content>
              <ul class="nav nav-third-level">
                  <@bpTree children=child.children />
              </ul>
              </#if>
              </li>
      </#list>
  </#if>
</#macro>