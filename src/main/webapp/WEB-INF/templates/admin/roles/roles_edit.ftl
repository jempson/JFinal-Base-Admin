
<form name="form1" class="form-horizontal" method="post" action="" id="addform">
	<input type="hidden" name="model.id" value="${model.id}" />
	<div class="form-group">
		<label for="type" class="col-sm-2 control-label">角色名称</label>
		<div class="col-sm-5">
			<input class="form-control" type="text" name="model.name" value="${model.name}" required />
		</div>
	</div>
	<div class="form-group">
		<label for="type" class="col-sm-2 control-label">描述</label>
		<div class="col-sm-5">
			<input class="form-control" type="text" name="model.remark" value="${model.remark}" required />
		</div>
	</div>
	<div class="text-center">
		<button class="btn btn-success" onclick="return oper_save(${model.id});">保 存</button>
	</div>
</form>

<script type="text/javascript">
	function oper_save(id) {
		if($("#addform").valid()){
			id = id || '0'
			form1.action = "${ctx}/admin/roles/save/"+id;
			form1.submit();
			return true;
		}
	}
	$("#addform").validate(
			{
				errorClass:'text-danger',
				errorElement: "span"
			});
	</script>
