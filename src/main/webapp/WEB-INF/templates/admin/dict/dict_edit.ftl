<!DOCTYPE html>
<html>
<head>
<@lib.adminhead/>

<script type="text/javascript">
		function oper_save(id) {
			if($("#addform").valid()){
				id = id || '0'
				form1.action = "${ctx}/admin/dict/save/"+id;
				form1.submit();
				return true;
			}
		}
	</script>

</head>
<body>
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header"><#if model.id?has_content>修改字典表<#else>新增字典表</#if></h1>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-default">
					<div class="panel-body">
						<form name="form1" class="form-horizontal" method="post" action="" id="addform">
							<input type="hidden" name="model.id" value="${model.id}" />
							<div class="form-group">
								<label for="type" class="col-sm-2 control-label">数据值</label>
								<div class="col-sm-5">
									<input class="form-control" type="text" name="model.value" value="${model.value}" required />
								</div>
							</div>
							<div class="form-group">
								<label for="type" class="col-sm-2 control-label">标签名</label>
								<div class="col-sm-5">
									<input class="form-control" type="text" name="model.label" value="${model.label}" required />
								</div>
							</div>
							<div class="form-group">
								<label for="type" class="col-sm-2 control-label">类型</label>
								<div class="col-sm-5">
									<input class="form-control" type="text" name="model.type" value="${model.type}" required />
								</div>
							</div>
							<div class="form-group">
								<label for="type" class="col-sm-2 control-label">排序（升序）</label>
								<div class="col-sm-5">
									<input class="form-control" type="text" name="model.sort" value="${model.sort}" required />
								</div>
							</div>
							<div class="form-group">
								<label for="type" class="col-sm-2 control-label">备注信息</label>
								<div class="col-sm-5">
									<input class="form-control" type="text" name="model.remarks" value="${model.remarks}" />
								</div>
							</div>
							<div class="text-center">
								<button class="btn btn-success" onclick="return oper_save(${model.id});">保 存</button>
							</div>
						</form>
						<hr />
					</div>
					<div class="panel-footer text-right">
						<a href="${ctx }/admin/dict/list" class="btn btn-default">列表</a>
					</div>
				</div>
			</div>
		</div>
	</div>
	<@lib.adminfootjs/>
	<script type="text/javascript">
	$("#addform").validate(
			{
				errorClass:'text-danger',
				errorElement: "span"
			});
	</script>
</body>
</html>
