package web.common.enums;


public enum ConstEnum {
	USER_SESSION("wxuser"), WX_GIRL_SESSION("wx_girl"), MIQ_USER_SESSION("miq_user_session"), MAP_KEY("_MAP"), LIST_KEY(
			"_LIST"), ID_KEY("_id_"), USER_ID_KEY(
			"_userid_"), OTHER_KEY("_other_key_");
	
	private String str;

	private ConstEnum(String str) {
		this.str = str;
	}

	public String getConst() {
		return str;
	}


}
