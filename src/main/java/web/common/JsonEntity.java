package web.common;

import java.io.Serializable;

import web.common.enums.ResultCodeEnum;

public class JsonEntity<T> implements Serializable {
	private static final long serialVersionUID = -6959001998411138451L;
	private int code; // 返回码，0为成功
	private String msg;
	private T data; // 单个对象

	public JsonEntity(ResultCodeEnum codeenum) {
		this.code = codeenum.getCode();
		this.msg = codeenum.getMessage();
	}


	public JsonEntity() {
		super();
		setJsonEntity(ResultCodeEnum.SUCCESS);
	}
	public T getData() {
		return data;
	}
	public void setData(T data) {
		this.data = data;
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public void setJsonEntity(ResultCodeEnum codeenum) {
		this.code = codeenum.getCode();
		this.msg = codeenum.getMessage();
	}
	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

}
