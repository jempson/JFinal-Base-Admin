package web.controller.admin;

import java.util.ArrayList;
import java.util.List;

import org.apache.shiro.authz.annotation.RequiresPermissions;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SimplePropertyPreFilter;

import kit.EhCacheKit;
import plugin.route.ControllerBind;
import web.common.BaseController;
import web.model.Permissions;
import web.model.temp.VoZtree;
import web.service.PermissionsService;
/**
 * 权限配置
 * 
 */
@ControllerBind(controllerKey = "/admin/permissions")
public class PermissionsController extends BaseController {

	private static final String path = "permissions_";
	private final static String CONTROLLER_KEY = "/admin/permissions";
	private static PermissionsService service = new PermissionsService();

	@RequiresPermissions("admin:perm:list")
	public void list() {
		List<VoZtree> znodes = service.getVoZtreeList();
		List<VoZtree> nodeList = new ArrayList<VoZtree>();
		for (VoZtree node1 : znodes) {
			boolean mark = false;
			for (VoZtree node2 : znodes) {
				if (node1.getPid() == node2.getId()) {
					mark = true;
					if (node2.getChildren() == null)
						node2.setChildren(new ArrayList<VoZtree>());
					node2.getChildren().add(node1);
					break;
				}
			}
			if (!mark) {
				nodeList.add(node1);
			}
		}
		SimplePropertyPreFilter filter = new SimplePropertyPreFilter();
		filter.getExcludes().add("checked");
		setAttr("znodes", JSON.toJSONString(nodeList, filter));
		render(path + "list.ftl");
	}


	@RequiresPermissions("admin:perm:add")
	public void add() {
		Long pid = getParaToLong("pid", 0L);
		Permissions model = new Permissions();
		model.setParentId(pid);
		setAttr("model", model);
		render(path + "add.ftl");
	}

	public void view() {
		Permissions model = Permissions.dao.findById(getParaToInt());
		setAttr("model", model);
		render(path + "view.ftl");
	}

	@RequiresPermissions("admin:perm:del")
	public void delete() {
		int id = getParaToInt(0, 0);
		if (id != 0) {
			boolean r = new Permissions().deleteById(id);
			if (r) {
				EhCacheKit.cleanRoleAndPerm();
				sendAlert("删除成功", CONTROLLER_KEY + "/list");
			} else {
				sendAlert("删除失败", CONTROLLER_KEY + "/list");
			}
		} else {
			sendAlert("删除失败", CONTROLLER_KEY + "/list");
		}
	}

	@RequiresPermissions("admin:perm:edit")
	public void edit() {
		int id = getParaToInt(0, 0);
		if (id > 0) {
			Permissions model = Permissions.dao.findById(id);
			setAttr("model", model);
			render(path + "edit.ftl");
		} else {
			sendAlert("操作失败:未找到id", CONTROLLER_KEY + "/list");
		}
	}

	@RequiresPermissions("admin:perm:add")
	public void save() {
		Integer pid = getParaToInt();
		Permissions model = getModel(Permissions.class,"model");
		boolean result =false;
		if (pid != null && pid > 0) { // 更新
			result =	model.update();
		} else { // 新增
			model.remove("id");
			result=	model.save();
		}
		if (result) {
			EhCacheKit.cleanRoleAndPerm();
			sendAlert("保存成功", CONTROLLER_KEY + "/list");
		} else {
			sendAlert("保存失败", CONTROLLER_KEY + "/list");
		}
	}

	/**
	* @Title: checkUnique
	* @Description: 检查唯一值
	* @author yangyw
	* @throws
	*/
	public void checkUnique() {
		int id = getParaToInt("id", 0);
		String perm = getPara("model.permission");
		List<Permissions> list = Permissions.dao.find("select * from permissions where permission=?", perm);
		boolean result = true;
		if (!list.isEmpty()) {
			if (id != 0) {
				for (Permissions permissions : list) {
					if (permissions.getId() != id) {
						result = false;
						break;
					}
				}
			} else {
				result = false;
			}
		}
		renderJson(result);
	}
}
